# FixFireBullets

Fixes shooting problems by sending positions displaced by one tick.

## Requirements

- [DHooks2](https://github.com/peace-maker/DHooks2)
